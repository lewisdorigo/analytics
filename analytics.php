<?php
/**
 * Plugin Name:       Analytics
 * Plugin URI:        https://bitbucket.org/lewisdorigo/analytics
 * Description:       Allows entry of analytics tracking codes, and provides users control to opt-out, if requested.
 * Version:           4.1.4
 * Author:            Lewis Dorigo
 * Author URI:        https://dorigo.co/
 */

use \Dorigo\Analytics\Analytics;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly.
}

// Define ANALYTICS_PLUGIN_FILE.
if(!defined('ANALYTICS_PLUGIN_FILE')) {
	define('ANALYTICS_PLUGIN_FILE', __FILE__ );
}


// Include the main Analytics class.
if(!class_exists('Dorigo\Analytics\Analytics')) {
	include_once dirname(__FILE__).'/lib/analytics.class.php';
}

add_action('plugins_loaded', function() {

    Analytics::Instance();

});

function drgo_analytics() {
    return Analytics::Instance();
}