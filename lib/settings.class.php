<?php namespace Dorigo\Analytics;

class Settings {
    private static $instance;

    public static function Instance() {
        if(is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct() {
        $this->registerMenuPage();
        $this->registerSettings();

        add_filter('acf/location/rule_values/options_page', [$this,'excludeOptionsFromList'], 11, 2);
        add_action('admin_head',[$this,'adminStyles'], 99999);
    }

    public function excludeOptionsFromList($values, $rule) {
        unset($values['dorigo-analytics']);

        if(empty($values) ) {
            $values[''] = __('No options pages exist', 'acf');
        }

        return $values;
    }

    public function adminStyles() {
        ?>
        <style>
            .drgo-analytics__mono input { font-family: monospace; }

            .drgo-analytics__title.acf-field .acf-label label { display: none; }
            .drgo-analytics__title.acf-field input {
                padding: 0 8px;
                font-size: 1.4em;
                height: 2em;
                line-height: 1.5em;
            }
        </style>
        <?php
    }

    private function registerMenuPage() {
        acf_add_options_sub_page(array(
            'page_title'     => 'Analytics &amp; Trackers',
            'menu_title'     => 'Analytics &amp; Trackers',
            'parent'         => 'options-general.php',
            'capability'	 => 'manage_options',
            'slug'           => 'dorigo-analytics'
        ));
    }

    private function registerSettings() {
        acf_add_local_field_group(array(
            'key' => 'group_5a95770300858',
            'title' => 'User Disclosure',
            'fields' => array(
                array(
                    'key' => 'field_5a95770ffa893',
                    'label' => 'User Disclosure',
                    'name' => '',
                    'type' => 'message',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'message' => '<p>If you are going to use tracking or analytics, you <em>must</em> have a privacy policy set up to inform users that they are being tracked. You can allow users to see all tracking scripts you have installed by adding the shortcode below to your privacy policy.</p>
                    <div class="acf-field drgo-analytics__mono drgo-analytics__title">
                        <div class="act-input">
                            <input type="text" value="[privacy-settings]" readonly="true" onfocus="this.select();" onmouseup="return false;">
                        </div>
                    </div>',
                    'new_lines' => 'wpautop',
                    'esc_html' => 0,
                ),
            ),
            'location' => array(
                array(
                    array(
                        'param' => 'options_page',
                        'operator' => '==',
                        'value' => 'dorigo-analytics',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'seamless',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));

        acf_add_local_field_group(array(
            'key' => 'group_5a9530f709173',
            'title' => 'Google Analytics',
            'fields' => array(
                array(
                    'key' => 'field_5a95310607f2c',
                    'label' => 'Google Analytics',
                    'name' => 'drgo_analytics_enable_ga',
                    'type' => 'true_false',
                    'instructions' => 'This does not currently support Google Analytics 4. To add Google Analytics 4 tracking, create a tracker below.',
                    'required' => 0,
                    'conditional_logic' => '',
                    'wrapper' => array(
                        'width' => '25',
                        'class' => '',
                        'id' => '',
                    ),
                    'message' => 'Enable Google Analytics?',
                    'default_value' => 0,
                    'ui' => 0,
                    'ui_on_text' => 'Enable',
                    'ui_off_text' => 'Disable',
                ),
                array(
                    'key' => 'field_5a95310607f2ca',
                    'label' => '&nbsp;',
                    'name' => 'drgo_analytics_ga_required',
                    'type' => 'true_false',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => array(
                        array(
                            array(
                                'field' => 'field_5a95310607f2c',
                                'operator' => '==',
                                'value' => '1',
                            ),
                        ),
                    ),
                    'wrapper' => array(
                        'width' => '25',
                        'class' => '',
                        'id' => '',
                    ),
                    'message' => 'Prevent opt-out for Google Analytics?',
                    'default_value' => 1,
                    'ui' => 0,
                    'ui_on_text' => 'Enable',
                    'ui_off_text' => 'Disable',
                ),
                array(
                    'key' => 'field_5a95313307f2d',
                    'label' => 'Tracking Code',
                    'name' => 'drgo_analytics_ga_code',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => array(
                        array(
                            array(
                                'field' => 'field_5a95310607f2c',
                                'operator' => '==',
                                'value' => '1',
                            ),
                        ),
                    ),
                    'wrapper' => array(
                        'width' => '50',
                        'class' => 'drgo-analytics__mono',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => 'UA-123456789-1',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
            ),
            'location' => array(
                array(
                    array(
                        'param' => 'options_page',
                        'operator' => '==',
                        'value' => 'dorigo-analytics',
                    ),
                ),
            ),
            'menu_order' => 1,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'field',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));

        acf_add_local_field_group(array(
            'key' => 'group_600007dde2a43',
            'title' => 'Cookie Bar',
            'fields' => array(
                array(
                    'key' => 'field_600007e3c7a16',
                    'label' => 'Cookie Bar',
                    'name' => 'drgo_analytics_cookie_bar_enable',
                    'type' => 'true_false',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'message' => 'Show the Cookie Bar',
                    'default_value' => 0,
                    'ui' => 0,
                    'ui_on_text' => '',
                    'ui_off_text' => '',
                ),
                array(
                    'key' => 'field_60000852c7a17',
                    'label' => 'Content',
                    'name' => 'drgo_analytics_cookie_bar_content',
                    'type' => 'textarea',
                    'instructions' => '(Max. 240 characters)',
                    'required' => 0,
                    'conditional_logic' => array(
                        array(
                            array(
                                'field' => 'field_600007e3c7a16',
                                'operator' => '==',
                                'value' => '1',
                            ),
                        ),
                    ),
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => 'We use cookies and other tools to ensure that we give you the best experience on our website.',
                    'placeholder' => '',
                    'maxlength' => 240,
                    'rows' => 2,
                    'new_lines' => '',
                ),
                array(
                    'key' => 'field_600008b2c7a18',
                    'label' => 'Allow All Button',
                    'name' => 'drgo_analytics_cookie_bar_allow_all',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => array(
                        array(
                            array(
                                'field' => 'field_600007e3c7a16',
                                'operator' => '==',
                                'value' => '1',
                            ),
                        ),
                    ),
                    'wrapper' => array(
                        'width' => 50,
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => 'Allow All',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_600008ecc7a19',
                    'label' => 'Manage Button',
                    'name' => 'drgo_analytics_cookie_bar_manage',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => array(
                        array(
                            array(
                                'field' => 'field_600007e3c7a16',
                                'operator' => '==',
                                'value' => '1',
                            ),
                        ),
                    ),
                    'wrapper' => array(
                        'width' => 50,
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => 'Manage',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
            ),
            'location' => array(
                array(
                    array(
                        'param' => 'options_page',
                        'operator' => '==',
                        'value' => 'dorigo-analytics',
                    ),
                ),
            ),
            'menu_order' => 2,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => true,
            'description' => '',
        ));


        acf_add_local_field_group(array(
            'key' => 'group_5a9531b994c76',
            'title' => 'Other Tracking Code',
            'fields' => array(
                array(
                    'key' => 'field_5a9531c145175',
                    'label' => 'Trackers',
                    'name' => 'drgo_analytics_trackers',
                    'type' => 'repeater',
                    'instructions' => 'To comply with GDPR, unless opting out is disabled for a tracker, the tracker will not be loaded unless a user specifically opts-in.',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'collapsed' => '',
                    'min' => 0,
                    'max' => 0,
                    'layout' => 'block',
                    'button_label' => 'Add Tracker',
                    'sub_fields' => array(
                        array(
                            'key' => 'field_5a95321945177',
                            'label' => 'Description',
                            'name' => '',
                            'type' => 'tab',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'placement' => 'left',
                            'endpoint' => 0,
                        ),
                        array(
                            'key' => 'field_5a9531e045176',
                            'label' => 'Title',
                            'name' => 'title',
                            'type' => 'text',
                            'instructions' => '',
                            'required' => 1,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => 'drgo-analytics__title',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => 'Enter the tracker name',
                            'prepend' => '',
                            'append' => '',
                            'maxlength' => '',
                        ),
                        array(
                            'key' => 'field_5a95322c45178',
                            'label' => 'Description',
                            'name' => 'tracker_description',
                            'type' => 'textarea',
                            'instructions' => 'A brief description of the what this tracking code is for.',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'maxlength' => '',
                            'rows' => 3,
                            'new_lines' => '',
                        ),
                        array(
                            'key' => 'field_5a95327045179',
                            'label' => 'Tracker Privacy Policy',
                            'name' => 'tracker_privacy',
                            'type' => 'url',
                            'instructions' => 'A link to the tracker’s privacy policy, so the user can be informed as to the tracker’s privacy practices.',
                            'required' => 1,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                        ),
                        array(
                            'key' => 'field_5a95330f56ea3',
                            'label' => 'Tracker Code',
                            'name' => '',
                            'type' => 'tab',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'placement' => 'left',
                            'endpoint' => 0,
                        ),
                        array(
                            'key' => 'field_5a95332656ea4',
                            'label' => 'Disable Opt-Out',
                            'name' => 'opt-out_disabled',
                            'type' => 'true_false',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'message' => 'Prevent the user from opting out of this tracker?',
                            'default_value' => 0,
                            'ui' => 0,
                            'ui_on_text' => '',
                            'ui_off_text' => '',
                        ),
                        array(
                            'key' => 'field_5a95336156ea5',
                            'label' => 'Load in Head',
                            'name' => 'load_in_head',
                            'type' => 'true_false',
                            'instructions' => 'Installing the tracker in the site’s header may affect performance by preventing the site from loading until the tracker has fully loaded.',
                            'required' => 0,
                            'conditional_logic' => array(
                                array(
                                    array(
                                        'field' => 'field_5a95332656ea4',
                                        'operator' => '==',
                                        'value' => '1',
                                    ),
                                ),
                            ),
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'message' => 'Install tracker in the site’s <code>&lt;head&gt;</code>?.',
                            'default_value' => 0,
                            'ui' => 0,
                            'ui_on_text' => '',
                            'ui_off_text' => '',
                        ),
                        array(
                            'key' => 'field_5a9532ab4517a',
                            'label' => 'Tracker Code',
                            'name' => 'tracker',
                            'type' => 'acf_code_field',
                            'instructions' => 'This should <strong>include</strong> any relevant <code>&lt;script&gt;</code> tags.',
                            'required' => 1,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'mode' => 'htmlmixed',
                            'theme' => 'monokai',
                        ),
                        array(
                            'key' => 'field_5a9532ab4517aa',
                            'label' => 'No-Script Code',
                            'name' => 'noscript',
                            'type' => 'acf_code_field',
                            'instructions' => 'This code will be added to the beginning of the <code>&lt;body&gt;</code>, and will only run if the user has JavaScript disabled.',
                            'required' => 0,
                            'conditional_logic' => array(
                                array(
                                    array(
                                        'field' => 'field_5a95332656ea4',
                                        'operator' => '==',
                                        'value' => '1',
                                    ),
                                ),
                            ),
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'mode' => 'htmlmixed',
                            'theme' => 'monokai',
                        ),
                    ),
                ),
            ),
            'location' => array(
                array(
                    array(
                        'param' => 'options_page',
                        'operator' => '==',
                        'value' => 'dorigo-analytics',
                    ),
                ),
            ),
            'menu_order' => 3,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'field',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));
    }
}
