<?php namespace Dorigo\Analytics;

class Analytics {
    public $version = '4.1.1';
    public $formAction = 'drgo-analytics-set-opt-in';

    public $templateId = 'analytics-controls';

    private static $instance;
    private $settings;
    private $trackers = [];
    private $debug;

    public static function Instance() {
        if(is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct() {
        if(function_exists('acf_add_local_field_group')) {

            $this->debug = defined('WP_DEBUG') && WP_DEBUG;
            add_action('init', [$this, 'setup']);

        } else {

            add_action('admin_notices', [$this, 'adminMessages']);
        }
    }

    public function setup() {
        $this->defineConstants();
        $this->includeSettings();
        $this->loadGoogleAnalytics();
        $this->loadTrackers();
        $this->addHooks();

        do_action('Dorigo\Analytics\loaded');
    }

    private function defineConstants() {
        $this->define('ANALYTICS_ABSPATH', dirname(ANALYTICS_PLUGIN_FILE ).'/');
        $this->define('ANALYTICS_PLUGIN_BASENAME', plugin_basename(ANALYTICS_PLUGIN_FILE));
        $this->define('ANALYTICS_VERSION', $this->version );
    }


    private function define($name, $value = true) {
        if(!defined($name)) {
            define($name, $value);
        }
    }

    private function includeSettings() {
        include_once ANALYTICS_ABSPATH.'/lib/settings.class.php';

        $this->settings = Settings::Instance();
    }

    public function adminMessages() {
        $class = 'notice notice-error';
        $message = __('Advanced Custom Fields isn’t installed. Please install Advanced Custom Fields to enable analytics.', 'sample-text-domain');

        printf('<div class="%1$s"><p>%2$s</p></div>', $class, $message);
    }

    private function loadGoogleAnalytics() {
        $includeAnalytics = get_field('drgo_analytics_enable_ga','options');
        $trackingCode = get_field('drgo_analytics_ga_code','options');
        $required = get_field('drgo_analytics_ga_required', 'options') !== false;

        if($includeAnalytics && $trackingCode) {

            $this->trackers[] = apply_filters('Dorigo\Analytics\GoogleAnalytics', [
                'title'          => 'Google Analytics',
                'slug'           => 'ga',
                'description'    => 'Google Analytics is a web analytics service offered by Google that tracks and reports website traffic.',
                'privacy_policy' => 'https://support.google.com/analytics/answer/6004245?hl=en',
                'opt_out'        => !$required,
                'in_header'      => false,
                'tracker_url'    => "https://www.googletagmanager.com/gtag/js?id={$trackingCode}",
                'code'           => "<script async src=\"https://www.googletagmanager.com/gtag/js?id={$trackingCode}\"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', '{$trackingCode}');
  gtag('set', {currency: 'GBP'});
</script>"
            ], $trackingCode);
        }
    }

    private function loadTrackers() {

        $trackers = null; //get_transient('drgo_analytics_trackers');

        if(!$trackers) {
            $trackers = get_field('drgo_analytics_trackers', 'options') ?: [];

            $trackers = array_map(function($tracker) {

                $title = $tracker['title'];
                $slug = sanitize_title($title);

                return apply_filters('Dorigo\Analytics\tracker='.$slug, [
                    'title'          => $title,
                    'slug'           => $slug,
                    'description'    => $tracker['tracker_description'],
                    'privacy_policy' => $tracker['tracker_privacy'],
                    'opt_out'        => !$tracker['opt-out_disabled'],
                    'in_header'      => $tracker['load_in_head'],
                    'code'           => $tracker['tracker'],
                    'noscript'       => $tracker['noscript'],
                ]);
            }, $trackers);

            set_transient('drgo_analytics_trackers', $trackers, HOUR_IN_SECONDS);
        }

        $this->trackers = apply_filters('Dorigo\Analytics\trackers', array_merge($this->trackers, $trackers));
        unset($trackers);
    }

    private function addHooks() {
        add_action('wp_head', [$this, 'addTrackers'], 0);
        add_action('wp_footer', [$this, 'addTrackers'], 99999);
        add_action('wp_footer', [$this, 'addCookieBar']);

        add_action('analytics_noscript', [$this, 'addNoscript'], 99999);

        add_action('wp_enqueue_scripts', [$this, 'enqueueJS']);
        add_action('acf/update_value/name=drgo_analytics_trackers', [$this, 'flushTransient'], 10, 1);

        add_shortcode('privacy-settings', [$this, 'shortcode']);
    }

    public function flushTransient($value) {
        delete_transient('drgo_analytics_trackers');

        return $value;
    }

    public function addNoscript() {

        $trackers = array_map(function($tracker) {
            return $tracker['opt_out'] === false && isset($tracker['noscript']) && $tracker['noscript'] ? $tracker['noscript'] : null;
        }, $this->trackers);

        $trackers = array_values(array_filter($trackers));

        if(!$trackers) { return; }

        echo '<noscript>';

        foreach($trackers as $tracker) {
            echo preg_replace('/<\/?noscript>/', '', $tracker);
        }

        echo '</noscript>';
    }

    public function addTrackers() {
        $isHead = current_action() && current_action() === 'wp_head';

        $trackers = array_map(function($tracker) use($isHead) {
            return $tracker['opt_out'] === false && $tracker['in_header'] === $isHead ? $tracker : null;
        }, $this->trackers);

        $trackers = array_values(array_filter($trackers));

        foreach($trackers as $tracker) {
            echo $tracker['code'];
        }
    }

    public function getTrackers() {
        return $this->trackers;
    }

    public function shortcode() {
        ob_start();

        if(locate_template(['analytics-shortcode.php'])) {
            get_template_part('analytics-shortcode');
        } else {
            include_once ANALYTICS_ABSPATH.'/templates/shortcode.php';
        }

        return ob_get_clean();
    }

    public function pluginUrl() {
        return untrailingslashit(plugins_url('/', ANALYTICS_PLUGIN_FILE));
    }

    public function enqueueJS() {
        wp_register_script('drgo-analytics-loader', $this->pluginUrl().'/assets/js/loader.min.js', ['jquery'], $this->version, true);


        $trackers = [];
        array_walk($this->trackers, function($tracker) use (&$trackers){

            if($tracker['opt_out']) {
                $trackers[$tracker['slug']] = $tracker;
            }

        });

        wp_localize_script('drgo-analytics-loader', 'drgo_analytics', [
            'trackers' => $trackers,
            'template_id' => $this->templateId,
            'privacy_page' => get_privacy_policy_url(),
        ]);

        wp_enqueue_script('drgo-analytics-loader');
    }

    public function getId() {
        return apply_filters('Dorigo/Analytics/TemplateId', $this->templateId);
    }

    public function addCookieBar() {
        if($this->getTrackers() && get_field('drgo_analytics_cookie_bar_enable', 'options')) {

            if(locate_template(['analytics-cookie-bar.php'])) {
                get_template_part('analytics-cookie-bar');
            } else {
                include_once ANALYTICS_ABSPATH.'/templates/cookie-bar.php';
            }

        }
    }

    public function cookieBarContent() {

        return [
            'content' => get_field('drgo_analytics_cookie_bar_content', 'options') ?: wpautop('We use cookies and other tools to ensure that we give you the best experience on our website.'),
            'allow_all' => get_field('drgo_analytics_cookie_bar_allow_all', 'options') ?: 'Allow All',
            'manage' => get_field('drgo_analytics_cookie_bar_manage', 'options') ?: 'Manage',
        ];
    }
}
