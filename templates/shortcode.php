<?php
    $analytics = drgo_analytics();
    $trackers = $analytics->getTrackers();
    $hasOptOut = false;

    if($trackers): ?>
<div id="<?= $analytics->getId(); ?>">
    <h2>Analytics &amp; Trackers</h2>
    <form action="<?= admin_url('/admin-ajax.php'); ?>" method="post" data-track>
        <ul class="drgo-trackers-list">
        <?php foreach($trackers as $tracker): ?>
            <li>
                <h3><?= $tracker['title']; ?></h3>
                <?= wpautop($tracker['description']); ?>

                <a href="<?= $tracker['privacy_policy']; ?>" target="_blank" rel="noopener"  data-track>Read the Privacy Policy</a>

                <?php if($tracker['opt_out']): ?>
                    <label>
                        <input type="checkbox" value="<?=$tracker['slug']; ?>" name="drgo-trackers-opt-in[]">
                        <span>Allow <?=$tracker['title'];?></span>
                    </label>
                    <?php $hasOptOut = true; ?>
                <?php endif; ?>
            </li>
        <?php endforeach; ?>
        </ul>

        <?php if($hasOptOut): ?>
        <button type="submit" class="accessibility">Refresh Opt-Outs</button>
        <?php endif; ?>

        <output class="accessibility" role="alert"></output>
    </form>
</div>
<?php endif;