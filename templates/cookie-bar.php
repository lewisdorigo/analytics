<?php
    $analytics = drgo_analytics();
    $content = $analytics->cookieBarContent();
?>
<aside class="drgo-cookie-bar" id="drgo-cookie-bar" style="display: none;">
    <div class="wrap wrap--wide drgo-cookie-bar__wrap">
        <div class="drgo-cookie-bar__content">
            <div class="drgo-cookie-bar__body">
                <?= $content['content']; ?>
            </div>

            <div class="drgo-cookie-bar__actions">
                <button type="button" class="button" id="drgo-cookie-bar__allow-all" data-track data-track-category="cookie-bar" data-track-label="allow-all">
                    <?= $content['allow_all']; ?>
                </button>

                <?php if($url = get_privacy_policy_url()): ?>
                <a href="<?= $url; ?>#<?= $analytics->getId(); ?>" class="button-outline" id="drgo-cookie-bar__manage" data-track data-track-category="cookie-bar" data-track-label="manage">
                    <?= $content['manage']; ?>
                </a>
                <?php endif; ?>

                <button type="button" class="outline-button" id="drgo-cookie-bar__dismiss" data-track data-track-category="cookie-bar" data-track-label="dismiss">
                    Dismiss
                </button>
            </div>
        </div>
    </div>
</aside>