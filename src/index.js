import $ from 'jquery';

let analytics = window.drgo_analytics || {},
    trackers = analytics.trackers || {};

const STORAGE_NAME = "drgo_tracker_opt_ins",
      COOKIE_BAR = "drgo_cookie_bar_dismiss",
      STORAGE = window.localStorage,
      TEMPLATE_ID = analytics.template_id || 'analytics-controls',
      PRIVACY_PAGE = analytics.privacy_page || '';

const $template = $(`#${TEMPLATE_ID}`);

let opt_ins = JSON.parse(STORAGE.getItem(STORAGE_NAME) || '[]'),
    loaded_trackers = [];

window.trackerFunctions = [

    (function(category, action, label, value) {

        if(typeof window.gtag !== 'function') {
            console.error('Failed to track event: Google Analytics does not exist.', object);
            return false;
        }

        value = value ? parseFloat(value) : null;

        window.gtag('event', action, {
            'event_category': category,
            'event_label': label,
            'value': value
        });

        console.log('Event tracked', {
            action: action,
            category: category,
            label: label,
            value: value,
        });

    }),
];

const triggerAnalytics = (function(category, action, label, value) {

    for(var i = 0; i < window.trackerFunctions.length; i++) {

        var tracker = window.trackerFunctions[i];

        if(typeof tracker === 'function') {
            tracker(category, action, label, value);
        }

    }

});

const loadTrackers = (function(announceTrackers) {
    $('output', $template).empty();

    $.each(trackers, function(slug, tracker) {
        if(opt_ins.indexOf(slug) >= 0) {

            if(loaded_trackers.indexOf(slug) < 0) {
                $("body").append(tracker.code);
                loaded_trackers.push(slug);

                console.log(slug+": loaded");
            } else {
                console.log(slug+": already loaded");
            }

            if(announceTrackers) {
                $('output', $template).append('<span>'+tracker.title+' has been enabled. <span>');
            }
        } else {
            console.log(slug+": skipped");
        }
    });

});

const hideCookieBar = (function() {

    STORAGE.setItem(COOKIE_BAR, true);
    $('#drgo-cookie-bar').fadeOut(250, function() {
        $(this).remove();
    });

});

const cookieBarInit = (function() {

    if(!STORAGE.getItem(COOKIE_BAR)) {
        $('#drgo-cookie-bar').prependTo('body').show();
    } else {
        $('#drgo-cookie-bar').remove();
    }

});

window.triggerAnalytics = triggerAnalytics;

loadTrackers();
cookieBarInit();

$('body').on('click', '#drgo-cookie-bar__dismiss', function(e) {
    e.preventDefault();
    hideCookieBar();
});

$('body').on('click', '#drgo-cookie-bar__allow-all', function(e) {
    e.preventDefault();

    opt_ins = new Array();

    $.each(trackers, function(slug, tracker) {
        opt_ins.push(slug);
    });

    STORAGE.setItem(STORAGE_NAME, JSON.stringify(opt_ins));
    loadTrackers();
    hideCookieBar();

});

$('body').on('click', '#drgo-cookie-bar__manage', function(e) {
    STORAGE.setItem(COOKIE_BAR, true);
    return true;
});

if($template.length > 0) {
    for(var i=0;i<opt_ins.length;i++) {
        $('input[name="drgo-trackers-opt-in[]"][value="'+opt_ins[i]+'"]').prop("checked", true);
    };

    $('input', $template).on('change', function(e) {
        $(this).closest('form').submit();
    });

    $('form', $template).on('submit', function(e) {
        e.preventDefault();

        var $this = $(this),
            $items = $('input[name="drgo-trackers-opt-in[]"]', $this);

        opt_ins = new Array();

        $items.each(function() {
            var $item = $(this),
                name = $item.attr('value');

            if($item.is(':checked')) {
                opt_ins.push(name);
            }
        });

        STORAGE.setItem(STORAGE_NAME, JSON.stringify(opt_ins));

        loadTrackers(true);
    });
}

$("body").on("click", "a[data-track], button[data-track]", function() {
    var category = this.getAttribute("data-track-category") || (this.tagName === "BUTTON" ? "button" : "link");
    var action   = this.getAttribute("data-track") || "click";
    var label    = this.getAttribute("data-track-label") || false;
    var value    = this.getAttribute("data-track-value") || null;

    if(label === false) {
        switch(this.tagName) {
            case "A":
                label = this.getAttribute("href");
                break;
            default:
                label = this.innerText;
                break;
        }
    }

    triggerAnalytics(category, action, label, value);
});

$("form[data-track]").on("submit", function() {

    if(typeof(window.ga) === "function") {

        var category = this.getAttribute("data-track-category") || "form";
        var action   = this.getAttribute("data-track") || "submit";
        var label    = this.getAttribute("data-track-label") || this.getAttribute("action");
        var value    = this.getAttribute("data-track-value") || null;

        triggerAnalytics(category, action, label, value);
    }

});
