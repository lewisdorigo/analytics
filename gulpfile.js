const gulp        = require('gulp'),
      babelify    = require('babelify'),
      browserify  = require('browserify'),
      plumber     = require('gulp-plumber'),
      uglify      = require('gulp-uglify'),
      buffer      = require('vinyl-buffer'),
      source      = require('vinyl-source-stream'),
      rename      = require('gulp-rename');

gulp.task('compile:js', (done) => {

  var bundleStream = browserify({
    debug: true,
    transform: [
      'babelify',
      'browserify-shim',
    ]
  }).add(`./src/index.js`).bundle();

  return bundleStream
    .pipe(plumber())
    .pipe(source('loader.min.js'))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(gulp.dest(`./assets/js`));
});
