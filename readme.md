# Analytics Plugin

This plugin adds an options page to WordPress, allowing analytics and tracking codes to be added to the site via the CMS.

If Google Analytics is enabled, basic event tracking is provided.

The ability for users to monitor and opt-out of tracking is provided through use of the `[privacy-settings]` shortcode.

## Privacy Settings
When the `[privacy-settings]` shortcode is included, that page will include a list of all trackers currently installed on the site, showing their name, description, and a link to the tracker’s privacy policy.

Where permitted, it will also display the option for the user to disable the tracker. Doing so will set a cookie and stop the tracking script from loading on subsequent page loads.

You can override the template used by the shortcode by including a `analytics-shortcode.php` file in your theme.

### Getting the Trackers
You can get an array of trackers with the following:
```php
<?php 
    $trackers = drgo_analytics()->getTrackers();
?>
```
This will return an array similar to this:
```
[
  [
    'title' => 'Google Analytics',
    'slug' => 'ga',
    'description' => 'Google Analytics is a web analytics service offered by Google that tracks and reports website traffic.',
    'privacy_policy' => 'https://support.google.com/analytics/answer/6004245?hl=en',
    'opt_out' => true,
    'in_header' => false,
    'tracker_url' => 'https://www.googletagmanager.com/gtag/js?id=UA-113900193-1',
    'code' => '<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113900193-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag(\'js\', new Date());

  gtag(\'config\', \'UA-113900193-1\');
</script>',
  ],
]
```

## Event Tracking
There is some rudimentary event tracking for Google Analytics built into the plugin. If Google Analytics is enabled.

The tracking only includes click events on `<a>` and `<button>` elements, and `<form>` submissions. Tracking events requires the element have the `data-track` attribute set.

The default tracking data submitted for each type is as follows:

| Element    | eventCategory | eventAction | eventLabel  |
|------------|---------------|-------------|-------------|
| `<a>`      | link          | click       | link href   |
| `<button>` | button        | click       | button text |
| `<form>`   | form          | submit      | form action |

For example, this link:
```
<a href="https://dorigo.co/" data-track>Visit My Wesbite</a>
```
will generate this event:

```json
{
    eventCategory : 'link',
    eventAction   : 'click',
    eventLabel    : 'https://dorigo.co/',
}
```
You can overwrite the eventAction by adding a value to the `data-track` attribute, and you can overwrite the eventLabel using the `data-track-label` attribute. For example:
```
<a href="#tab-panel-1" data-track="toggle" data-label="Accordion Panel 1">Open Accordion</a>
```
